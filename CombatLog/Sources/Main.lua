if not GetLocale() == "zhCN" then
    exit()
end

local playerGUID = UnitGUID("player")
local frame = CreateFrame("Frame")
local events = {
    ["SPELL_CAST_SUCCESS"] = function(...)
        frame:OnSpellCastSuccessEvent(...)
    end,
    ["SPELL_INTERRUPT"] = function(...)
        frame:OnSpellInterruptEvent(...)
    end,
}

frame:RegisterEvent("COMBAT_LOG_EVENT_UNFILTERED")
frame:SetScript("OnEvent", function(self, _)
    self:OnEvent(CombatLogGetCurrentEventInfo())
end)

function frame:OnEvent(...)
    local event = select(2, ...)
    local invoke = events[event]
    if (invoke) then
        invoke(...)
    end
end

function frame:OnSpellCastSuccessEvent(...)
    local destGUID = select(8, ...)
    if not (destGUID and destGUID == playerGUID) then
        return
    end
    local spellId, spellName = select(12, ...)
    local sourceSpell = spellId and GetSpellLink(spellId) or spellName
    local sourceGUID = select(4, ...)
    if sourceGUID and sourceGUID == playerGUID then
        print(string.format("你对自己施放了%s", sourceSpell))
    else
        local sourceName = select(5, ...)
        local displaySourceName = string.gsub(sourceName, "-.+$", "")
        print(string.format("%s对你施放了%s", displaySourceName, sourceSpell))
    end
end

function frame:OnSpellInterruptEvent(...)
    local sourceGUID = select(4, ...)
    local destGUID = select(8, ...)
    if not ((sourceGUID and sourceGUID == playerGUID) or (destGUID and destGUID == playerGUID)) then
        return
    end
    local sourceName = select(5, ...)
    local displaySourceName = string.gsub(sourceName, "-.+$", "")
    if sourceGUID and sourceGUID == playerGUID then
        displaySourceName = "你"
    end
    local spellId, spellName, _, extraSpellId, extraSpellName, _ = select(12, ...)
    local sourceSpell = spellId and GetSpellLink(spellId) or spellName
    local destName = select(9, ...)
    local displayDestName = string.gsub(destName, "-.+$", "")
    if destGUID and destGUID == playerGUID then
        displayDestName = "你"
    end
    local destSpell = extraSpellId and GetSpellLink(extraSpellId) or extraSpellName
    print(string.format("%s的%s打断了%s的%s", displaySourceName, sourceSpell, displayDestName, destSpell))
end
